<?php

namespace App\Entity;

class SubmissionStatus
{
    const SUBMITTED = 'submitted';
    const ACCEPTED = 'accepted';
    const REJECTED = 'rejected';
    const WITHDRAWN = 'withdrawn';

}