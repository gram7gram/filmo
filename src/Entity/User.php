<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="users")
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface, \Serializable
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @JMS\Groups("api_v1")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=64, nullable=false)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $birthday;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_active", type="boolean", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $isActive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     */
    private $publicToken;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_admin", type="boolean", nullable=false)
     */
    private $isAdmin;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=128, unique=true, nullable=true)
     */
    private $passwordToken;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $passwordTokenRequestedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $passwordTokenExpiresAt;

    public function __construct()
    {
        $this->isActive = true;
        $this->isAdmin = false;
        $this->createdAt = new \DateTime();
    }

    public function refreshPasswordToken()
    {
        $this->passwordToken = hash('sha256', uniqid());
        $this->passwordTokenRequestedAt = new \DateTime();
        $this->passwordTokenExpiresAt = new \DateTime();
        $this->passwordTokenExpiresAt->modify("+24 hours");
    }

    public function getUsername()
    {
        return $this->getEmail();
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function getRoles()
    {
        $roles = [Role::USER];

        if ($this->isAdmin) {
            $roles[] = Role::ADMIN;
        }

        return $roles;
    }

    public function isActive()
    {
        return $this->isActive;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getPublicToken(): ?string
    {
        return $this->publicToken;
    }

    /**
     * @param string $publicToken
     */
    public function setPublicToken(?string $publicToken): void
    {
        $this->publicToken = $publicToken;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->isAdmin;
    }

    public function getSalt()
    {
        return null;
    }

    public function eraseCredentials()
    {
        $this->publicToken = null;
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->email,
            $this->password,
            $this->isActive,
            $this->isAdmin,
        ));
    }

    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->email,
            $this->password,
            $this->isActive,
            $this->isAdmin,
            ) = unserialize($serialized, array('allowed_classes' => false));
    }

    public function __toString()
    {
        return $this->getUsername();
    }

    /**
     * @return string
     */
    public function getPasswordToken(): ?string
    {
        return $this->passwordToken;
    }

    /**
     * @param string $passwordToken
     */
    public function setPasswordToken(?string $passwordToken): void
    {
        $this->passwordToken = $passwordToken;
    }

    /**
     * @return \DateTime
     */
    public function getPasswordTokenExpiresAt(): ?\DateTime
    {
        return $this->passwordTokenExpiresAt;
    }

    /**
     * @param \DateTime $passwordTokenExpiresAt
     */
    public function setPasswordTokenExpiresAt(?\DateTime $passwordTokenExpiresAt): void
    {
        $this->passwordTokenExpiresAt = $passwordTokenExpiresAt;
    }

    /**
     * @return \DateTime
     */
    public function getPasswordTokenRequestedAt(): ?\DateTime
    {
        return $this->passwordTokenRequestedAt;
    }

    /**
     * @param \DateTime $passwordTokenRequested
     */
    public function setPasswordTokenRequestedAt(?\DateTime $passwordTokenRequested): void
    {
        $this->passwordTokenRequestedAt = $passwordTokenRequested;
    }

    /**
     * @return bool
     */
    public function isPasswordTokenExpired()
    {
        if (!$this->passwordTokenExpiresAt) return true;

        $now = new \DateTime();

        return $now->diff($this->passwordTokenExpiresAt)->h >= 24;
    }

    /**
     * @return \DateTime
     */
    public function getBirthday(): ?\DateTime
    {
        return $this->birthday;
    }

    /**
     * @param \DateTime $birthday
     */
    public function setBirthday(?\DateTime $birthday): void
    {
        $this->birthday = $birthday;
    }
}