<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Table(name="submissions")
 * @ORM\Entity(repositoryClass="App\Repository\SubmissionRepository")
 */
class Submission
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $createdAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $user;

    /**
     * @var Movie
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Movie")
     * @ORM\JoinColumn(nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $movie;

    /**
     * @var Category
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     * @ORM\JoinColumn(nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $videoName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $statusUpdatedAt;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $statusUpdatedBy;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     *
     * @JMS\Groups("api_v1")
     */
    private $isWithdrawn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @JMS\Groups("api_v1")
     */
    private $withdrawnAt;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->statusUpdatedAt = new \DateTime();
        $this->isWithdrawn = false;
        $this->status = SubmissionStatus::SUBMITTED;
    }

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return User
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @return Movie
     */
    public function getMovie(): ?Movie
    {
        return $this->movie;
    }

    /**
     * @param Movie $movie
     */
    public function setMovie(Movie $movie): void
    {
        $this->movie = $movie;
    }

    /**
     * @return Category
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory(Category $category): void
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getStatusUpdatedAt(): ?\DateTime
    {
        return $this->statusUpdatedAt;
    }

    /**
     * @param \DateTime $statusUpdatedAt
     */
    public function setStatusUpdatedAt(\DateTime $statusUpdatedAt): void
    {
        $this->statusUpdatedAt = $statusUpdatedAt;
    }

    /**
     * @return User
     */
    public function getStatusUpdatedBy(): ?User
    {
        return $this->statusUpdatedBy;
    }

    /**
     * @param User $statusUpdatedBy
     */
    public function setStatusUpdatedBy(User $statusUpdatedBy): void
    {
        $this->statusUpdatedBy = $statusUpdatedBy;
    }

    /**
     * @return bool
     */
    public function isWithdrawn(): bool
    {
        return $this->isWithdrawn;
    }

    /**
     * @param bool $isWithdrawn
     */
    public function setIsWithdrawn(bool $isWithdrawn): void
    {
        $this->isWithdrawn = $isWithdrawn;
    }

    /**
     * @return \DateTime
     */
    public function getWithdrawnAt(): \DateTime
    {
        return $this->withdrawnAt;
    }

    /**
     * @param \DateTime $withdrawnAt
     */
    public function setWithdrawnAt(\DateTime $withdrawnAt): void
    {
        $this->withdrawnAt = $withdrawnAt;
    }

    /**
     * @return string
     */
    public function getVideoName(): ?string
    {
        return $this->videoName;
    }

    /**
     * @param string $videoName
     */
    public function setVideoName(string $videoName): void
    {
        $this->videoName = $videoName;
    }

}