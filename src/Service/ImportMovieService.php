<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImportMovieService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function import(UploadedFile $file)
    {
        $service = $this->container->get(MovieService::class);
        $em = $this->container->get('doctrine')->getManager();

        $root = $this->container->getParameter('kernel.project_dir') . '/public';
        $posterDirectory = $this->container->getParameter('poster_dir');
        $outputFile = '/downloads/' . md5(uniqid()) . '.csv';
        $outputPath = $root . $outputFile;

        $rows = array_map('str_getcsv', file($file->getPathname()));

        $responseErrors = [];
        $errors = [];

        foreach ($rows as $index => &$row) {
            if ($index === 0) continue;

            $id = isset($row[0]) && $row[0] ? trim($row[0]) : null;
            $name = isset($row[1]) ? trim($row[1]) : null;
            $category = isset($row[2]) ? trim($row[2]) : null;
            $isHidden = isset($row[3]) ? intval(trim($row[3])) === 1 : null;
            $posterName = isset($row[4]) ? trim($row[4]) : null;
            $row[5] = null; //row with errors

            $em->beginTransaction();

            try {

                $content = [
                    'name' => $name,
                    'category' => [
                        'name' => $category
                    ],
                    'isHidden' => $isHidden,
                ];

                $posterFile = null;
                if ($posterName) {
                    $path = $root . $posterDirectory . '/' . $posterName;

                    if (!file_exists($path)) {
                        throw new \Exception('Poster not found', 404);
                    }

                    $posterFile = new UploadedFile($path, $posterName, null, UPLOAD_ERR_OK, true);
                }

                if ($id) {
                    $movie = $service->find($id);
                    if (!$movie) {
                        throw new \Exception('Movie was not found by id: ' . $id, 404);
                    }

                    $service->update($movie, $content, $posterFile, false);
                } else {
                    $service->create($content, $posterFile, false);
                }

                $em->commit();

            } catch (\Exception $e) {

                $em->rollback();

                $row[5] = $e->getMessage();

                $errors[] = $row;

                $responseErrors[] = [
                    'row' => $index + 1,
                    'error' => $e->getMessage()
                ];
            }
        }

        if (!$responseErrors) {
            return null;
        }

        $output = fopen($outputPath, 'w');

        foreach ($errors as $row) {
            fputcsv($output, $row);
        }

        fclose($output);

        return [
            'output' => $outputFile,
            'errors' => $responseErrors,
        ];
    }

    public function validate(UploadedFile $file)
    {

        if (mb_strtolower($file->getClientOriginalExtension(), 'utf8') !== 'csv') {
            throw new \Exception("Not a CSV file. Tried: *." . $file->getClientOriginalExtension(), 400);
        }

        if (!in_array(mb_strtolower($file->getClientMimeType(), 'utf8'), ['text/plain', 'text/csv', 'application/vnd.ms-excel'])) {
            throw new \Exception("Not a CSV file: Tried: " . $file->getClientMimeType(), 400);
        }

        $rows = array_map('str_getcsv', file($file->getPathname()));

        if (count($rows) < 2) {
            throw new \Exception("No rows in csv file", 400);
        }

        $header = $rows[0];
        if (count($header) !== 5 || implode(',', $header) !== 'ID,Name,Category,IsHidden,PosterName') {
            throw new \Exception("Invalid import template", 400);
        }

        $responseErrors = [];

        foreach ($rows as $index => $row) {
            if ($index === 0) continue;

            $id = isset($row[0]) && $row[0] ? trim($row[0]) : null;
            $name = isset($row[1]) ? trim($row[1]) : null;
            $category = isset($row[2]) ? trim($row[2]) : null;
            $isHidden = isset($row[3]) ? intval(trim($row[3])) : null;
            $posterName = isset($row[4]) ? trim($row[4]) : null;

            try {

                if ($id && (!is_numeric($id) || intval($id) <= 0)) {
                    throw new \Exception("Invalid movie id. Should be a positive number", 400);
                }

                if (is_null($name) || !$name) {
                    throw new \Exception("Name is required", 400);
                }

                if (is_null($category) || !$category) {
                    throw new \Exception("Category is required", 400);
                }

                if (is_null($posterName) || !$posterName) {
                    throw new \Exception("Poster is required", 400);
                }

                if (is_null($isHidden) || !in_array($isHidden, [1, 0])) {
                    throw new \Exception("isHidden should be one of: 1, 0", 400);
                }

            } catch (\Exception $e) {
                $responseErrors[] = [
                    'row' => $index + 1,
                    'error' => $e->getMessage()
                ];
            }
        }

        if (!$responseErrors) return null;

        return [
            'errors' => $responseErrors,
        ];
    }

}
