<?php

namespace App\Service;

use App\Entity\Category;
use App\Entity\Movie;
use App\Entity\SubmissionStatus;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MovieService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function exportByFilter($filter)
    {
        $total = $this->countByFilter($filter);

        if (!$total) return null;

        $movies = $this->findByFilterWithMetadata($filter);

        $root = $this->container->getParameter('kernel.project_dir') . '/public';
        $downloadsDirectory = $this->container->getParameter('downloads_dir');
        $file = md5(uniqid()) . '.csv';

        $path = $root . $downloadsDirectory . '/' . $file;

        $handle = fopen($path, 'w');

        $trans = $this->container->get('translator');

        fputcsv($handle, [
            'ID',
            $trans->trans('name'),
            $trans->trans('submission_count', ['_placeholder_' => 'total']),
            $trans->trans('submission_count', ['_placeholder_' => SubmissionStatus::ACCEPTED]),
            $trans->trans('submission_count', ['_placeholder_' => SubmissionStatus::WITHDRAWN]),
        ]);

        foreach ($movies as $content) {
            /** @var Movie $movie */
            $movie = $content[0];

            $content = [
                $movie->getId(),
                $movie->getName(),
                $content['submissionTotalCount'],
                $content['submissionAcceptedCount'],
                $content['submissionWithdrawnCount'],
            ];

            fputcsv($handle, $content);
        }

        fclose($handle);

        return $downloadsDirectory . '/' . $file;
    }

    /**
     * @param $content
     * @param UploadedFile $file
     * @param bool $shouldCheckFile
     * @return Movie
     * @throws \Exception
     */
    public function create($content, UploadedFile $file, $shouldCheckFile = true)
    {
        $user = $this->container->get(UserService::class)->getUser();

        $movie = new Movie();
        $movie->setCreatedBy($user);

        $this->update($movie, $content, $file, $shouldCheckFile);

        return $movie;
    }

    /**
     * @param Movie $movie
     * @param $content
     * @param UploadedFile $file
     * @param bool $shouldCheckFile
     * @throws \Exception
     */
    public function update(Movie $movie, $content, UploadedFile $file = null, $shouldCheckFile = true)
    {
        $em = $this->container->get('doctrine')->getManager();

        if (isset($content['category']['name'])) {
            $category = $em->getRepository(Category::class)->findOneBy([
                'name' => $content['category']['name']
            ]);
            if (!$category) {
                throw new \Exception("Category was not found by name: " . $content['category']['name'], 404);
            }

            $movie->setCategory($category);

        } elseif (isset($content['category'])) {
            $category = $em->getRepository(Category::class)->find($content['category']);
            if (!$category) {
                throw new \Exception("Category was not found by id: " . $content['category'], 404);
            }

            $movie->setCategory($category);
        }

        if ($file) {

            $root = $this->container->getParameter('kernel.project_dir') . '/public';
            $posterDirectory = $this->container->getParameter('poster_dir');
            $posterMimeTypes = explode(',', $this->container->getParameter('upload_poster_mime_types'));

            if ($shouldCheckFile && array_search($file->getClientMimeType(), $posterMimeTypes) === false) {
                throw new \Exception("Poster mime type not allowed", 400);
            }

            $name = $file->getClientOriginalName();

            $file->move($root . $posterDirectory, $name);

            $movie->setPosterName($name);
        }

        if (isset($content['name'])) {
            $movie->setName($content['name']);
        }

        if (isset($content['isHidden'])) {
            $movie->setIsHidden($content['isHidden'] === true);
        }

        $em->persist($movie);

        $em->flush();
    }

    /**
     * @param array $filter
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Movie::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     * @param array $order
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function findByFilter(array $filter = [], array $order = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Movie::class)->findByFilter($filter, $order, $page, $limit);
    }

    /**
     * @param array $filter
     * @param array $order
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function findByFilterWithMetadata(array $filter = [], array $order = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Movie::class)->findByFilterWithMetadata($filter, $order, $page, $limit);
    }

    /**
     * @param $id
     * @return null|Movie
     */
    public function find($id)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Movie::class)->find($id);
    }

    public function serialize($content)
    {
        return json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);
    }


}
