<?php

namespace App\Service;

use App\Entity\Movie;
use App\Entity\Submission;
use App\Entity\SubmissionStatus;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SubmissionService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function exportByFilter($filter)
    {
        $submissions = $this->findByFilter($filter);
        if (!$submissions) return null;

        $root = $this->container->getParameter('kernel.project_dir') . '/public';
        $downloadsDirectory = $this->container->getParameter('downloads_dir');
        $file = md5(uniqid()) . '.csv';

        $path = $root . $downloadsDirectory . '/' . $file;

        $handle = fopen($path, 'w');

        $trans = $this->container->get('translator');

        fputcsv($handle, [
            'ID',
            $trans->trans('category'),
            $trans->trans('movie'),
            $trans->trans('status'),
            $trans->trans('submission.status_date'),
            $trans->trans('submission.video_name'),
        ]);

        /** @var Submission $submission */
        foreach ($submissions as $submission) {

            $submissions = [
                $submission->getId(),
                $submission->getCategory()->getName(),
                $submission->getMovie()->getName(),
                $submission->getStatus(),
                $submission->getStatusUpdatedAt() ? $submission->getStatusUpdatedAt()->format('Y-m-d H:i:s') : '-',
                $submission->getVideoName()
            ];

            fputcsv($handle, $submissions);
        }

        fclose($handle);

        return $downloadsDirectory . '/' . $file;
    }

    /**
     * @param $content
     * @param UploadedFile $file
     * @return Submission
     * @throws \Exception
     */
    public function create($content, UploadedFile $file)
    {
        $em = $this->container->get('doctrine')->getManager();
        $user = $this->container->get(UserService::class)->getUser();

        $root = $this->container->getParameter('kernel.project_dir') . '/public';
        $videoDirectory = $this->container->getParameter('submission_dir');
        $submissionMimeTypes = explode(',', $this->container->getParameter('upload_submission_mime_types'));

        $entity = new Submission();
        $entity->setUser($user);

        if (array_search($file->getClientMimeType(), $submissionMimeTypes) === false) {
            throw new \Exception("Video mime type not allowed", 400);
        }

        $videoName = md5(uniqid()) . '.' . $file->getClientOriginalExtension();

        $file->move($root . $videoDirectory, $videoName);

        $entity->setVideoName($videoName);

        if (isset($content['movie'])) {
            /** @var Movie $movie */
            $movie = $em->getRepository(Movie::class)->find($content['movie']);
            if (!$movie) {
                throw new \Exception("Movie was not found", 404);
            }

            $entity->setMovie($movie);
            $entity->setCategory($movie->getCategory());
        }

        $this->update($entity, $content);

        return $entity;
    }

    /**
     * @param Submission $entity
     * @param $content
     * @throws \Exception
     */
    public function update(Submission $entity, $content)
    {
        $em = $this->container->get('doctrine')->getManager();
        $user = $this->container->get(UserService::class)->getUser();
        $now = new \DateTime();

        if (isset($content['status'])) {

            switch ($content['status']) {
                case SubmissionStatus::ACCEPTED:
                case SubmissionStatus::REJECTED:
                case SubmissionStatus::WITHDRAWN:
                    $entity->setStatus($content['status']);
                    break;
                default:
                    throw new \Exception("Invalid submisison status", 400);
            }

            $entity->setStatusUpdatedAt($now);
            $entity->setStatusUpdatedBy($user);

            switch ($entity->getStatus()) {
                case SubmissionStatus::WITHDRAWN:
                    $content['isWithdrawn'] = true;
                    break;
            }
        }

        if (isset($content['isWithdrawn'])) {
            $entity->setIsWithdrawn($content['isWithdrawn'] === true);
            $entity->setWithdrawnAt($now);

            if ($entity->isWithdrawn()) {
                $entity->setStatus(SubmissionStatus::WITHDRAWN);
                $entity->setStatusUpdatedAt($now);
                $entity->setStatusUpdatedBy($user);
            }
        }

        $em->persist($entity);

        $em->flush();
    }

    /**
     * @param array $filter
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Submission::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function findByFilter(array $filter = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Submission::class)->findByFilter($filter, $page, $limit);
    }

    /**
     * @param $id
     * @return null|Submission
     */
    public function find($id)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(Submission::class)->find($id);
    }

    public function serialize($content)
    {
        return json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);
    }


}
