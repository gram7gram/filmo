<?php

namespace App\Service;

use App\Entity\User;
use JMS\Serializer\SerializationContext;
use Symfony\Component\DependencyInjection\ContainerInterface;

class UserService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param $content
     *
     * @return User
     * @throws \Exception
     */
    public function create($content)
    {
        $trans = $this->container->get('translator');
        $currentUser = $this->container->get(UserService::class)->getUser();

        $isAdmin = $currentUser && $currentUser->isAdmin();

        if (!isset($content['email'])) {
            throw new \Exception($trans->trans('validation.bad_request'), 400);
        }

        if (!$isAdmin && !isset($content['birthday'])) {
            throw new \Exception($trans->trans('validation.bad_request'), 400);
        }

        $entity = new User();
        $entity->setPublicToken(md5(uniqid()));

        $this->update($entity, $content);

        return $entity;
    }

    /**
     * @param User $entity
     * @param $content
     *
     * @throws \Exception
     */
    public function update(User $entity, $content)
    {
        $em = $this->container->get('doctrine')->getManager();
        $encoder = $this->container->get('security.password_encoder');
        $trans = $this->container->get('translator');
        $currentUser = $this->container->get(UserService::class)->getUser();

        if (isset($content['email'])) {
            $entity->setEmail(mb_strtolower(trim($content['email']), 'utf8'));
        }

        if (isset($content['name'])) {
            $entity->setName(trim($content['name']));
        }

        if (isset($content['isActive'])) {
            $entity->setIsActive($content['isActive'] === true);
        }

        if (isset($content['password'])) {

            if ($currentUser && !$currentUser->isAdmin() && isset($content['currentPassword'])) {
                $isValid = $encoder->isPasswordValid($entity, $content['currentPassword']);
                if (!$isValid) {
                    throw new \Exception($trans->trans('validation.current_password_mismatch'), 400);
                }
            }

            $password = $encoder->encodePassword($entity, $content['password']);
            $entity->setPassword($password);
        }

        if (isset($content['birthday'])) {
            $date = \DateTime::createFromFormat("Y-m-d", $content['birthday']);
            if (!$date) {
                throw new \Exception($trans->trans('validation.bad_request'), 400);
            }

            $now = new \DateTime();

            if ($now->diff($date)->y < 18) {
                throw new \Exception($trans->trans('users.age_error'), 400);
            }

            $entity->setBirthday($date);
        }

        $this->validate($entity);

        $em->persist($entity);
        $em->flush();
    }

    private function validate(User $entity)
    {
        $trans = $this->container->get('translator');
        $em = $this->container->get('doctrine')->getManager();

        $match = $em->getRepository(User::class)->findOneBy([
            'email' => $entity->getEmail(),
        ]);
        if ($match && $match !== $entity) {
            throw new \Exception($trans->trans('validation.email_reserved'), 400);
        }
    }

    /**
     * @param array $filter
     *
     * @return int
     * @throws \Exception
     */
    public function countByFilter(array $filter = [])
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(User::class)->countByFilter($filter);
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     *
     * @return array
     */
    public function findByFilter(array $filter = [], $page = 0, $limit = 0)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(User::class)->findByFilter($filter, $page, $limit);
    }

    /**
     * @param $filter
     *
     * @return User|null
     */
    public function findOneByFilter($filter)
    {
        $items = $this->findByFilter($filter, 1, 1);
        if (count($items) !== 1) return null;

        return $items[0];
    }

    /**
     * @param $id
     *
     * @return null|User
     */
    public function find($id)
    {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository(User::class)->find($id);
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The SecurityBundle is not registered in your application. Try running "composer require symfony/security-bundle".');
        }

        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return null;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return null;
        }

        return $user;
    }

    public function serialize($content)
    {
        return json_decode($this->container->get('jms_serializer')
            ->serialize($content, 'json', SerializationContext::create()
                ->setGroups(['api_v1'])), true);
    }


}
