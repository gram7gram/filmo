<?php

namespace App\Service;


use Symfony\Component\DependencyInjection\ContainerInterface;

class CaptchaService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function isEnabled()
    {
        return $this->container->getParameter('google_recaptcha_enabled');
    }

    public function isValid($token)
    {
        $secret = $this->container->getParameter('google_recaptcha_secret_key');

        $c = curl_init();

        curl_setopt($c, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
        curl_setopt($c, CURLOPT_POST, 1);
        curl_setopt($c, CURLOPT_POSTFIELDS,
            implode('&', [
                'secret=' . $secret,
                'response=' . $token
            ])
        );

        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($c);

        curl_close($c);

        $content = json_decode($response, true);

        return isset($content['success']) && $content['success'] === true;
    }

}