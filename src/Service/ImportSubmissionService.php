<?php

namespace App\Service;

use App\Service\SubmissionService;
use App\Entity\SubmissionStatus;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImportSubmissionService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function import(UploadedFile $file)
    {
        $service = $this->container->get(SubmissionService::class);

        $root = $this->container->getParameter('kernel.project_dir') . '/public';
        $outputFile = '/downloads/' . md5(uniqid()) . '.csv';
        $outputPath = $root . $outputFile;

        $rows = array_map('str_getcsv', file($file->getPathname()));

        $errors = [];
        $responseErrors = [];

        foreach ($rows as $index => $row) {
            if ($index === 0) continue;

            $id = isset($row[0]) && $row[0] ? trim($row[0]) : null;
            $status = isset($row[1]) ? trim($row[1]) : null;
            $row[2] = null; //for errors

            try {
                $content = [
                    'status' => $status,
                ];

                $submission = $service->find($id);
                if (!$submission) {
                    throw new \Exception('Submission was not found. id: ' . $id, 404);
                }

                if (!$submission->isWithdrawn()) {
                    $service->update($submission, $content);
                }

            } catch (\Exception $e) {

                $row[2] = $e->getMessage();

                $errors[] = $row;

                $responseErrors[] = [
                    'row' => $index + 1,
                    'error' => $e->getMessage()
                ];
            }
        }

        if (!$errors) {
            return null;
        }

        $output = fopen($outputPath, 'w');

        foreach ($errors as $row) {
            fputcsv($output, $row);
        }

        fclose($output);

        return [
            'output' => $outputFile,
            'errors' => $responseErrors,
        ];
    }

    public function validate(UploadedFile $file)
    {
        if (mb_strtolower($file->getClientOriginalExtension(), 'utf8') !== 'csv') {
            throw new \Exception("Not a CSV file. Tried: *." . $file->getClientOriginalExtension(), 400);
        }

        if (!in_array(mb_strtolower($file->getClientMimeType(), 'utf8'), ['text/plain', 'text/csv', 'application/vnd.ms-excel'])) {
            throw new \Exception("Not a CSV file: Tried: " . $file->getClientMimeType(), 400);
        }

        $rows = array_map('str_getcsv', file($file->getPathname()));

        if (count($rows) < 2) {
            throw new \Exception("No rows in csv file", 400);
        }

        $header = $rows[0];
        if (count($header) !== 2 || implode(',', $header) !== 'ID,Status') {
            throw new \Exception("Invalid import template", 400);
        }

        $responseErrors = [];

        foreach ($rows as $index => $row) {
            if ($index === 0) continue;

            $id = isset($row[0]) && $row[0] ? trim($row[0]) : null;
            $status = isset($row[1]) ? trim($row[1]) : null;

            try {
                if (!$id || !is_numeric($id) || intval($id) <= 0) {
                    throw new \Exception("Invalid submission id. Should be a positive number", 400);
                }

                if (!$status) {
                    throw new \Exception("Submission status is required", 400);
                } else {
                    switch ($status) {
                        case SubmissionStatus::ACCEPTED:
                        case SubmissionStatus::REJECTED:
                        case SubmissionStatus::WITHDRAWN:
                            break;
                        default:
                            throw new \Exception("Unknown submission status", 400);
                    }
                }
            } catch (\Exception $e) {
                $responseErrors[] = [
                    'row' => $index + 1,
                    'error' => $e->getMessage()
                ];
            }
        }

        if (!$responseErrors) return null;

        return [
            'errors' => $responseErrors,
        ];
    }

}
