<?php

namespace App\Service;

use App\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;

class EmailService
{

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function sentResetPassword(User $user)
    {
        if (!$user->getEmail()) return;

        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        $title = $trans->trans('email.reset_password.title');

        $body = $twig->render('email/reset-password.html.twig', [
            'user' => $user
        ]);

        $this->notify($title, $user->getEmail(), $body);
    }

    public function sendActivationEmail(User $user)
    {

        $trans = $this->container->get('translator');
        $twig = $this->container->get('twig');

        $title = $trans->trans('email.activation.title');

        $body = $twig->render('email/activation.html.twig', [
            'user' => $user
        ]);

        $this->notify($title, $user->getEmail(), $body);
    }

    public function notify($title, $to, $body)
    {
        $isValid = preg_match('/.+\@.+\..+/', $to);
        if (!$isValid) return;

        $mailer = $this->container->get('mailer');
        $from = $this->container->getParameter('mailer_sender_name');

        $message = new \Swift_Message();
        $message->setFrom($from)
            ->setTo($to)
            ->setSubject($title)
            ->setBody($body, 'text/html');

        $mailer->send($message);
    }

}