<?php

namespace App\Controller;

use App\Entity\Submission;
use App\Service\CategoryService;
use App\Service\MovieService;
use App\Service\SubmissionService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SubmissionController extends Controller
{

    public function indexAction(Request $request)
    {
        $submissionService = $this->get(SubmissionService::class);
        $movieService = $this->get(MovieService::class);
        $categoryService = $this->get(CategoryService::class);
        $userService = $this->get(UserService::class);

        $user = $userService->getUser();
        $isAdmin = $user->isAdmin();

        $filter = $request->get('filter', []);
        $page = intval($request->get('page', 1));
        $page = $page > 0 ? $page : 1;
//        $limit = intval($request->get('limit', 10));
        $limit = 10;

        if (!$isAdmin) {
            $filter = [
                'user' => $user->getId()
            ];
        }

        $submissions = [];

        $total = $submissionService->countByFilter($filter);

        $pages = ceil($total / $limit);

        if ($total > 0) {
            $submissions = $submissionService->findByFilter($filter, $page, $limit);
        }

        if ($isAdmin) {

            $categories = $categoryService->findByFilter();
            $movies = $movieService->findByFilter();
            $users = $userService->findByFilter();

            return $this->render('submissions-admin.html.twig', [
                'filter' => $filter,
                'filter_json' => str_replace('&quot;', '"', json_encode($filter)),
                'page' => $page,
                'pages' => $pages,
                'limit' => $limit,
                'total' => $total,
                'count' => count($submissions),
                'submissions' => $submissions,
                'categories' => $categories,
                'movies' => $movies,
                'users' => $users,
            ]);
        } else {
            return $this->render('submissions.html.twig', [
                'page' => $page,
                'pages' => $pages,
                'limit' => $limit,
                'total' => $total,
                'count' => count($submissions),
                'submissions' => $submissions,
            ]);
        }
    }

    public function newAction($id)
    {
        $movieService = $this->get(MovieService::class);

        $movie = $movieService->find($id);
        if (!$movie || $movie->isHidden()) {
            throw $this->createNotFoundException();
        }

        return $this->render('submission-new.html.twig', [
            'movie' => $movie,
            'submission' => new Submission(),
        ]);
    }

    public function editAction($id)
    {
        $service = $this->get(SubmissionService::class);

        $submission = $service->find($id);
        if (!$submission) {
            throw $this->createNotFoundException();
        }

        return $this->render('submission-show.html.twig', [
            'submission' => $submission,
        ]);
    }

}