<?php

namespace App\Controller;

use App\Entity\Role;
use App\Service\SubmissionService;
use App\Service\ImportSubmissionService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SubmissionRESTController extends Controller
{

    public function getsAction(Request $request)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $service = $this->get(SubmissionService::class);

        $filter = $request->get('filter', []);
        $page = intval($request->get('page', 1));
        $page = $page > 0 ? $page : 1;
        $limit = intval($request->get('limit', 10));
        $limit = $limit > 0 ? $limit : 10;

        try {

            $total = $service->countByFilter($filter);
            $items = [];

            if ($total > 0) {
                $entities = $service->findByFilter($filter, $page, $limit);

                $items = $service->serialize($entities);
            }

            return new JsonResponse([
                'page' => $page,
                'limit' => $limit,
                'count' => count($items),
                'total' => $total,
                'items' => $items
            ]);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getAction($id)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $service = $this->get(SubmissionService::class);

        try {

            $entity = $service->find($id);
            if (!$entity) {
                return new JsonResponse([
                    'message' => 'Not found'
                ], JsonResponse::HTTP_NOT_FOUND);
            }

            $item = $service->serialize($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function putAction(Request $request, $id)
    {

        $userService = $this->get(UserService::class);
        $service = $this->get(SubmissionService::class);

        try {

            $entity = $service->find($id);
            if (!$entity) {
                return new JsonResponse([
                    'message' => 'Not found'
                ], JsonResponse::HTTP_NOT_FOUND);
            }

            $currentUser = $userService->getUser();
            if (!$currentUser->isAdmin()) {
                if ($currentUser !== $entity->getUser()) {
                    throw $this->createAccessDeniedException();
                }
            }

            $content = json_decode($request->getContent(), true);

            $service->update($entity, $content);

            $item = $service->serialize($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postAction(Request $request)
    {
        $service = $this->get(SubmissionService::class);

        $file = $request->files->get('video');
        $formData = $request->get('content');
        if (!$formData) {
            $formData = $request->getContent();
        }

        $formData = json_decode($formData, true);
        $content = isset($formData['content']) ? $formData['content'] : $formData;

        if (!($file && $content)) {
            return new JsonResponse([
                'message' => 'Missing file and content'
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        try {

            $entity = $service->create($content, $file);

            $item = $service->serialize($entity);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function importAction(Request $request)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $importService = $this->get(ImportSubmissionService::class);

        /** @var UploadedFile $file */
        $file = $request->files->get('import');
        if (!$file) {
            return new JsonResponse([
                'message' => 'Missing file'
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        try {
            $errors = $importService->validate($file);
            if ($errors) {
                return new JsonResponse($errors, JsonResponse::HTTP_BAD_REQUEST);
            }

            $errors = $importService->import($file);
            if ($errors) {
                return new JsonResponse($errors, JsonResponse::HTTP_BAD_REQUEST);
            }

            return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300
                ? $e->getCode()
                : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function exportAction(Request $request)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $service = $this->get(SubmissionService::class);

        $content = json_decode($request->getContent(), true);

        $filter = isset($content['filter']) ? $content['filter'] : [];

        $path = $service->exportByFilter($filter);
        if (!$path) {
            return new JsonResponse([
                'message' => 'Not found'
            ], JsonResponse::HTTP_NOT_FOUND);
        }

        return new JsonResponse([
            'url' => $path
        ], JsonResponse::HTTP_CREATED);
    }

    public function obsoleteAction()
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $service = $this->get(SubmissionService::class);
        $days = $this->getParameter('submission_obsolete_days');
        $em = $this->getDoctrine()->getManager();

        $submissions = $service->findByFilter([
            'obsolete' => $days
        ]);
        if (!$submissions) {
            return new JsonResponse([], JsonResponse::HTTP_NO_CONTENT);
        }

        $em->beginTransaction();

        try {
            foreach ($submissions as $submission) {
                $em->remove($submission);
            }

            $em->flush();

            $em->commit();

            return new JsonResponse([], JsonResponse::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            $em->rollback();

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300
                ? $e->getCode()
                : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);

        }
    }

}
