<?php

namespace App\Controller;

use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class IndexController extends Controller
{

    public function entryAction()
    {
        return $this->redirect($this->generateUrl('movies'));
    }

    public function login()
    {
        return $this->render('login.html.twig');
    }

    public function register()
    {
        $user = $this->get(UserService::class)->getUser();
        if ($user) {
            return $this->redirect($this->generateUrl('entry'));
        }

        return $this->render('register.twig');
    }

    public function indexAction()
    {
        return $this->render('index.html.twig');
    }

    public function termsAction()
    {
        return $this->render('terms.html.twig');
    }

    public function tutorialAction()
    {
        return $this->render('tutorial.html.twig');
    }

    public function contactUs()
    {
        return $this->render('contact-us.html.twig');
    }

    public function contactSupport()
    {
        return $this->render('contact-support.html.twig');
    }

    public function faq()
    {
        return $this->render('faq.html.twig');
    }

}
