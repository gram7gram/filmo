<?php

namespace App\Controller;

use App\Entity\Role;
use App\Service\MovieService;
use App\Service\ImportMovieService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class MovieRESTController extends Controller
{

    public function getsAction(Request $request)
    {
        $service = $this->get(MovieService::class);

        $filter = $request->get('filter', []);
        $page = intval($request->get('page', 1));
        $page = $page > 0 ? $page : 1;
        $limit = intval($request->get('limit', 10));
        $limit = $limit > 0 ? $limit : 10;

        try {

            $total = $service->countByFilter($filter);
            $items = [];

            if ($total > 0) {
                $movies = $service->findByFilter($filter, [], $page, $limit);

                $items = $service->serialize($movies);
            }

            return new JsonResponse([
                'page' => $page,
                'limit' => $limit,
                'count' => count($items),
                'total' => $total,
                'items' => $items
            ]);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getAction($id)
    {
        $service = $this->get(MovieService::class);

        try {

            $movie = $service->find($id);
            if (!$movie) {
                return new JsonResponse([
                    'message' => 'Not found'
                ], JsonResponse::HTTP_NOT_FOUND);
            }

            $item = $service->serialize($movie);

            return new JsonResponse($item);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function putAction(Request $request, $id)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $file = $request->files->get('poster');
        $formData = $request->get('content');
        if (!$formData) {
            $formData = $request->getContent();
        }

        $formData = json_decode($formData, true);
        $content = isset($formData['content']) ? $formData['content'] : $formData;

        $service = $this->get(MovieService::class);

        try {

            $movie = $service->find($id);
            if (!$movie) {
                return new JsonResponse([
                    'message' => 'Not found'
                ], JsonResponse::HTTP_NOT_FOUND);
            }

            $service->update($movie, $content, $file);

            $item = $service->serialize($movie);

            return new JsonResponse($item);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postAction(Request $request)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $file = $request->files->get('poster');
        $formData = $request->get('content');
        if (!$formData) {
            $formData = $request->getContent();
        }

        $formData = json_decode($formData, true);
        $content = isset($formData['content']) ? $formData['content'] : $formData;

        if (!($file && $content)) {
            return new JsonResponse([
                'message' => 'Missing file and content'
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        $service = $this->get(MovieService::class);

        try {

            $movie = $service->create($content, $file);

            $item = $service->serialize($movie);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function hideAction(Request $request)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $formData = json_decode($request->getContent(), true);
        $movies = isset($formData['movies']) ? $formData['movies'] : null;
        $isSelectAll = isset($formData['isSelectAll']) ? $formData['isSelectAll'] : false;
        $filter = isset($formData['filter']) ? $formData['filter'] : null;

        $service = $this->get(MovieService::class);
        $em = $this->getDoctrine()->getManager();

        $em->beginTransaction();
        try {

            $count = 0;
            if ($isSelectAll) {
                $movies = $service->findByFilter(array_merge($filter, [
                    'isHidden' => false
                ]));
                foreach ($movies as $movie) {
                    $service->update($movie, [
                        'isHidden' => true
                    ]);

                    ++$count;
                }
            } else {

                foreach ($movies as $id) {
                    $movie = $service->find($id);
                    if (!$movie) {
                        throw $this->createNotFoundException();
                    }

                    $service->update($movie, [
                        'isHidden' => true
                    ]);

                    ++$count;
                }
            }

            $em->commit();

            return new JsonResponse([
                'message' => 'ok',
                'count' => $count
            ]);

        } catch (\Exception $e) {

            $em->rollback();

            return new JsonResponse([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function importAction(Request $request)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $importService = $this->get(ImportMovieService::class);

        /** @var UploadedFile $file */
        $file = $request->files->get('import');
        if (!$file) {
            return new JsonResponse([
                'message' => 'Missing file'
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        try {

            $errors = $importService->validate($file);
            if ($errors) {
                return new JsonResponse($errors, JsonResponse::HTTP_BAD_REQUEST);
            }

            $errors = $importService->import($file);
            if ($errors) {
                return new JsonResponse($errors, JsonResponse::HTTP_BAD_REQUEST);
            }

            return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300
                ? $e->getCode()
                : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function exportAction(Request $request)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $service = $this->get(MovieService::class);

        $content = json_decode($request->getContent(), true);

        $filter = isset($content['filter']) ? $content['filter'] : [];

        $path = $service->exportByFilter($filter);
        if (!$path) {
            return new JsonResponse([
                'message' => 'Not found'
            ], JsonResponse::HTTP_NOT_FOUND);
        }

        return new JsonResponse([
            'url' => $path
        ], JsonResponse::HTTP_CREATED);
    }

}
