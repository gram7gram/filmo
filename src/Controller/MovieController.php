<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Entity\Role;
use App\Service\CategoryService;
use App\Service\MovieService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class MovieController extends Controller
{

    public function indexAction(Request $request)
    {
        $movieService = $this->get(MovieService::class);
        $categoryService = $this->get(CategoryService::class);
        $userService = $this->get(UserService::class);

        $user = $userService->getUser();
        $isAdmin = $user->isAdmin();

        $filter = $request->get('filter', []);
        $page = intval($request->get('page', 1));
        $page = $page > 0 ? $page : 1;

        $limit = $isAdmin ? 10 : 24;

        $categories = $categoryService->findByFilter();

        if (!$filter && $categories) {
            $category = $categories[0];
            $filter = [
                'category' => $category->getId()
            ];
        }

        if (!$isAdmin) {
            $orders = [
                'totalSubmissions' => 'DESC',
                'id' => 'DESC'
            ];
        } else {
            $orders = [
                'id' => 'DESC'
            ];
        }

        if ($isAdmin) {

            $total = $movieService->countByFilter($filter);

            $pages = ceil($total / $limit);

            $movies = $movieService->findByFilterWithMetadata($filter, $orders, $page, $limit);

            return $this->render('movies-admin.html.twig', [
                'categories' => $categories,
                'filter' => $filter,
                'filter_json' => str_replace('&quot;', '"', json_encode($filter)),
                'page' => $page,
                'pages' => $pages,
                'limit' => $limit,
                'total' => $total,
                'count' => count($movies),
                'movies' => $movies
            ]);
        } else {

            $publicFilter = array_merge($filter, [
                'isHidden' => false
            ]);

            $total = $movieService->countByFilter($publicFilter);

            $pages = ceil($total / $limit);

            $movies = $movieService->findByFilter($publicFilter, $orders, $page, $limit);

            return $this->render('movies.html.twig', [
                'categories' => $categories,
                'filter' => $filter,
                'filter_json' => str_replace('&quot;', '"', json_encode($filter)),
                'page' => $page,
                'pages' => $pages,
                'limit' => $limit,
                'total' => $total,
                'count' => count($movies),
                'movies' => $movies
            ]);
        }
    }

    public function newAction()
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $categoryService = $this->get(CategoryService::class);
        $categories = $categoryService->findByFilter();

        return $this->render('movie-new.html.twig', [
            'categories' => $categories,
            'movie' => new Movie(),
        ]);
    }

    public function editAction($id)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $service = $this->get(MovieService::class);

        $movie = $service->find($id);
        if (!$movie) {
            throw $this->createNotFoundException();
        }

        $categoryService = $this->get(CategoryService::class);
        $categories = $categoryService->findByFilter();

        return $this->render('movie-show.html.twig', [
            'categories' => $categories,
            'movie' => $movie,
        ]);
    }

}