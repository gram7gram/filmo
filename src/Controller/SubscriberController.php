<?php

namespace App\Controller;

use App\Entity\Role;
use App\Entity\User;
use App\Service\SubmissionService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SubscriberController extends Controller
{

    public function indexAction(Request $request)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $userService = $this->get(UserService::class);

        $filter = $request->get('filter', []);
        $page = intval($request->get('page', 1));
        $page = $page > 0 ? $page : 1;
        $limit = intval($request->get('limit', 10));
        $limit = $limit > 0 ? $limit : 10;

        $users = [];

        $total = $userService->countByFilter($filter);

        $pages = ceil($total / $limit);

        if ($total > 0) {
            $users = $userService->findByFilter($filter, $page, $limit);
        }

        return $this->render('subscribers.html.twig', [
            'filter' => $filter,
            'filter_json' => str_replace('&quot;', '"', json_encode($filter)),
            'page' => $page,
            'pages' => $pages,
            'limit' => $limit,
            'total' => $total,
            'count' => count($users),
            'users' => $users
        ]);
    }

    public function newAction()
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        return $this->render('subscribers-new.html.twig', [
            'user' => new User()
        ]);
    }

    public function editAction($id)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $service = $this->get(UserService::class);
        $submissionService = $this->get(SubmissionService::class);

        $isAdmin = $service->getUser()->isAdmin();

        $user = $service->find($id);
        if (!$user) {
            throw $this->createNotFoundException();
        }

        $submissions = $submissionService->findByFilter([
            'user' => $user->getId()
        ]);

        if ($isAdmin) {
            return $this->render('subscribers-show-admin.html.twig', [
                'user' => $user,
                'submissions' => $submissions,
            ]);
        } else {
            return $this->render('subscribers-show.html.twig', [
                'user' => $user,
                'submissions' => $submissions,
            ]);
        }
    }

    public function profile()
    {
        $service = $this->get(UserService::class);
        $submissionService = $this->get(SubmissionService::class);

        $user = $service->getUser();
        if (!$user) {
            throw $this->createNotFoundException();
        }

        $submissions = $submissionService->findByFilter([
            'user' => $user->getId()
        ]);

        return $this->render('subscribers-show.html.twig', [
            'user' => $user,
            'submissions' => $submissions,
        ]);
    }

}
