<?php

namespace App\Controller;

use App\Service\CaptchaService;
use App\Service\EmailService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class RegisterRESTController extends Controller
{
    /**
     * recaptcha protected route
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function postAction(Request $request)
    {
        $service = $this->get(UserService::class);
        $emailService = $this->get(EmailService::class);
        $captchaService = $this->get(CaptchaService::class);
        $trans = $this->get('translator');

        $content = json_decode($request->getContent(), true);

        try {

            if ($captchaService->isEnabled()) {

                if (!isset($content['g-recaptcha-response'])) {
                    throw new \Exception($trans->trans('validation.bad_request'), 400);
                }

                $isValid = $captchaService->isValid($content['g-recaptcha-response']);
                if (!$isValid) {
                    throw new \Exception($trans->trans('validation.you_are_a_robot'), 400);
                }
            }

            $content['isActive'] = false;

            $entity = $service->create($content);

            try {
                $emailService->sendActivationEmail($entity);
            } catch (\Exception $e) {
                throw new \Exception("Activation email could not be send", 500);
            }

            $item = $service->serialize($entity);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}