<?php

namespace App\Controller;

use App\Service\CaptchaService;
use App\Service\EmailService;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PasswordRESTController extends Controller
{
    /**
     * recaptcha protected route
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function postResetAction(Request $request)
    {
        $trans = $this->get('translator');
        $service = $this->get(UserService::class);
        $email = $this->get(EmailService::class);
        $captchaService = $this->get(CaptchaService::class);

        $content = json_decode($request->getContent(), true);

        if (!isset($content['login'])) {
            return new JsonResponse([
                'message' => $trans->trans('validation.bad_request')
            ], JsonResponse::HTTP_BAD_REQUEST);
        }

        $em = $this->get('doctrine')->getManager();

        try {

            $user = $service->findOneByFilter([
                'login' => $content['login']
            ]);
            if (!$user) {
               throw new \Exception($trans->trans('validation.not_found'), 404);
            }

            $requestedAt = $user->getPasswordTokenRequestedAt();

            if ($requestedAt) {
                $now = new \DateTime();

                if ($now->diff($requestedAt)->i < 15) {
                    throw new \Exception($trans->trans('reset_password.error_to_may_requests'), 400);
                }
            }

            if ($captchaService->isEnabled()) {

                if (!isset($content['g-recaptcha-response'])) {
                    throw new \Exception($trans->trans('validation.bad_request'), 400);
                }

                $isValid = $captchaService->isValid($content['g-recaptcha-response']);
                if (!$isValid) {
                    throw new \Exception($trans->trans('validation.bad_request'), 400);
                }
            }

            $user->refreshPasswordToken();

            $em->persist($user);
            $em->flush();

            $email->sentResetPassword($user);

            $item = $service->serialize($user);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function putSetAction(Request $request, $token)
    {
        $trans = $this->get('translator');
        $service = $this->get(UserService::class);

        $user = $service->findOneByFilter([
            'passwordToken' => $token,
        ]);
        if (!$user || $user->isPasswordTokenExpired()) {
            return new JsonResponse([
                'message' => $trans->trans('validation.not_found')
            ], JsonResponse::HTTP_NOT_FOUND);
        }

        $content = json_decode($request->getContent(), true);

        try {

            $user->setPasswordToken(null);
            $user->setPasswordTokenExpiresAt(null);
            $user->setPasswordTokenRequestedAt(null);

            $service->update($user, $content);

            $item = $service->serialize($user);

            return new JsonResponse($item);

        } catch (\Exception $e) {

            return new JsonResponse([
                'message' => $e->getMessage()
            ], $e->getCode() > 300 ? $e->getCode() : JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}