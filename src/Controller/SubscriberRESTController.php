<?php

namespace App\Controller;

use App\Entity\Role;
use App\Service\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SubscriberRESTController extends Controller
{

    public function getsAction(Request $request)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $service = $this->get(UserService::class);

        $filter = $request->get('filter', []);
        $page = intval($request->get('page', 1));
        $page = $page > 0 ? $page : 1;
        $limit = intval($request->get('limit', 10));
        $limit = $limit > 0 ? $limit : 10;

        try {

            $total = $service->countByFilter($filter);
            $items = [];

            if ($total > 0) {
                $entities = $service->findByFilter($filter, $page, $limit);

                $items = $service->serialize($entities);
            }

            return new JsonResponse([
                'page' => $page,
                'limit' => $limit,
                'count' => count($items),
                'total' => $total,
                'items' => $items
            ]);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function getAction($id)
    {
        $this->denyAccessUnlessGranted(Role::ADMIN);

        $service = $this->get(UserService::class);

        try {

            $entity = $service->find($id);
            if (!$entity) {
                return new JsonResponse([
                    'message' => 'Not found'
                ], JsonResponse::HTTP_NOT_FOUND);
            }

            $item = $service->serialize($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function putAction(Request $request, $id)
    {
        $service = $this->get(UserService::class);

        try {

            $entity = $service->find($id);
            if (!$entity) {
                return new JsonResponse([
                    'message' => 'Not found'
                ], JsonResponse::HTTP_NOT_FOUND);
            }

            $currentUser = $service->getUser();
            if (!$currentUser->isAdmin()) {
                if ($currentUser !== $entity) {
                    throw $this->createAccessDeniedException();
                }
            }

            $content = json_decode($request->getContent(), true);

            $service->update($entity, $content);

            $item = $service->serialize($entity);

            return new JsonResponse($item);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function postAction(Request $request)
    {

        $this->denyAccessUnlessGranted(Role::ADMIN);

        $service = $this->get(UserService::class);
        $content = json_decode($request->getContent(), true);

        try {

            $entity = $service->create($content);

            $item = $service->serialize($entity);

            return new JsonResponse($item, JsonResponse::HTTP_CREATED);

        } catch (\Exception $e) {
            return new JsonResponse([
                'message' => $e->getMessage()
            ], JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}