<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LoginController extends Controller
{

    public function logout()
    {
        //empty body
    }

    /**
     * recaptcha protected route
     */
    public function authenticate()
    {
        //empty body
    }

    public function activateAction($token)
    {
        $em = $this->getDoctrine()->getManager();

        try {

            $user = $em->getRepository(User::class)->findOneBy([
                'isActive' => false,
                'publicToken' => $token
            ]);
            if ($user) {
                $user->setPublicToken(null);
                $user->setIsActive(true);

                $em->persist($user);
                $em->flush();
            }

            return $this->redirect($this->generateUrl('login'));
        } catch (\Exception $e) {
            throw $this->createNotFoundException();
        }
    }
}