<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PasswordController extends Controller
{

    public function setPassword($token)
    {
        $em = $this->getDoctrine()->getManager();

        try {

            $user = $em->getRepository(User::class)->findOneBy([
                'passwordToken' => $token
            ]);
            if (!$user || $user->isPasswordTokenExpired()) {
                return $this->redirect($this->generateUrl('login'));
            }

            return $this->render('set-password.html.twig', [
                'user' => $user
            ]);
        } catch (\Exception $e) {
            throw $this->createNotFoundException();
        }
    }

    public function resetPassword()
    {
        return $this->render('reset-password.html.twig');
    }
}