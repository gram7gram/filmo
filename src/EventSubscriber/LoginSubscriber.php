<?php

namespace App\EventSubscriber;

use App\Service\CaptchaService;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;

class LoginSubscriber implements EventSubscriberInterface
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents()
    {
        return array(
            SecurityEvents::INTERACTIVE_LOGIN => ['onLogin', 20],
        );
    }

    public function onLogin(InteractiveLoginEvent $event)
    {
        $request = $event->getRequest();

        $captchaService = $this->container->get(CaptchaService::class);
        $trans = $this->container->get('translator');

        if ($captchaService->isEnabled()) {

            $content = json_decode($request->getContent(), true);

            if (!isset($content['g-recaptcha-response'])) {
                throw new AuthenticationException($trans->trans('validation.bad_request'));
            }

            $isValid = $captchaService->isValid($content['g-recaptcha-response']);
            if (!$isValid) {
                throw new AuthenticationException($trans->trans('validation.you_are_a_robot'));
            }
        }

    }
}