<?php

namespace App\Repository;

use App\Entity\Movie;
use App\Entity\SubmissionStatus;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityRepository;

class MovieRepository extends EntityRepository
{
    /**
     * @param mixed $id
     * @param null $lockMode
     * @param null $lockVersion
     * @return Movie|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        $qb = $this->createFilterQuery();
        $e = $qb->expr();

        $qb->where($e->eq('movie.id', ':id'))
            ->setParameter('id', $id);

        return $qb->getQuery()
            ->useQueryCache(true)
            ->getOneOrNullResult();
    }

    /**
     * @param array $filter
     * @param array $order
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function findByFilterWithMetadata($filter, $order, $page, $limit)
    {
        $qb = $this->createFilterQuery($filter, $order);

        $qb->addSelect("(
            SELECT COUNT(DISTINCT submission1.id) 
            FROM App\Entity\Submission submission1
            INNER JOIN App\Entity\Movie innerMovie1 WITH innerMovie1.id = movie.id AND innerMovie1.id = submission1.movie
        ) AS submissionTotalCount");

        $qb->addSelect("(
            SELECT COUNT(DISTINCT submission2.id) 
            FROM App\Entity\Submission submission2
            INNER JOIN App\Entity\Movie innerMovie2 WITH innerMovie2.id = movie.id AND innerMovie2.id = submission2.movie
            WHERE submission2.status = '" . SubmissionStatus::ACCEPTED . "'
        ) AS submissionAcceptedCount");

        $qb->addSelect("(
            SELECT COUNT(DISTINCT submission3.id) 
            FROM App\Entity\Submission submission3
            INNER JOIN App\Entity\Movie innerMovie3 WITH innerMovie3.id = movie.id AND innerMovie3.id = submission3.movie
            WHERE submission3.status = '" . SubmissionStatus::WITHDRAWN . "'
        ) AS submissionWithdrawnCount");

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        return $qb->getQuery()
            ->useQueryCache(true)
//            ->useResultCache(true, 3600 * 24, md5(json_encode($filter) . $page . $limit))
            ->getResult();
    }

    /**
     * @param array $filter
     * @param $order
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function findByFilter($filter, $order, $page, $limit)
    {
        $qb = $this->createFilterQuery($filter, $order);

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        return $qb->getQuery()
            ->useQueryCache(true)
//            ->useResultCache(true, 3600 * 24, md5(json_encode($filter) . $page . $limit))
            ->getResult();
    }

    private function createFilterQuery($filter = [], $order = null)
    {
        $qb = $this->createQueryBuilder('movie');
        $e = $qb->expr();

        $qb
            ->addSelect('category')
            ->addSelect('createdBy');

        $qb
            ->join('movie.category', 'category')
            ->join('movie.createdBy', 'createdBy');

        foreach ($filter as $key => $value) {
            switch ($key) {
                case 'isHidden':
                    $qb->andWhere($e->eq('movie.isHidden', ":$key"))
                        ->setParameter($key, $value, Type::BOOLEAN);
                    break;
                case 'category':
                    $qb->andWhere($e->eq('category.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'letter':
                    $qb->andWhere($e->like($e->lower('movie.name'), ":$key"))
                        ->setParameter($key, mb_strtolower($value, 'utf8') . '%');
                    break;
                case 'search':
                    $qb->andWhere($e->like($e->lower('movie.name'), ":$key"))
                        ->setParameter($key, mb_strtolower($value, 'utf8') . '%');
                    break;
                case 'acceptedCount':

                    if (isset($filter['acceptedType'])) {

                        $inner = 'key_' . md5(uniqid());

                        // Select submission count with specific status for a movie
                        $query = "(
                            SELECT COUNT(DISTINCT ${inner}_submission.id) 
                            FROM App\Entity\Submission ${inner}_submission
                            INNER JOIN App\Entity\Movie ${inner}_movie WITH ${inner}_movie.id = movie.id AND ${inner}_movie.id = ${inner}_submission.movie
                            WHERE ${inner}_submission.status = '" . SubmissionStatus::ACCEPTED . "'
                        )";

                        switch ($filter['acceptedType']) {
                            case 'eq':
                                $qb->andWhere($e->eq($query, ":$key"));
                                break;
                            case 'more':
                                $qb->andWhere($e->gt($query, ":$key"));
                                break;
                            case 'more-eq':
                                $qb->andWhere($e->gte($query, ":$key"));
                                break;
                            case 'less-eq':
                                $qb->andWhere($e->lte($query, ":$key"));
                                break;
                            case 'less':
                                $qb->andWhere($e->lt($query, ":$key"));
                                break;
                            default:
                                continue;
                        }

                        $qb->setParameter($key, $value);

                    }
                    break;
                case 'totalCount':

                    if (isset($filter['totalType'])) {

                        $inner = 'key_' . md5(uniqid());

                        // Select submission count for a movie
                        $query = "(
                            SELECT COUNT(DISTINCT ${inner}_submission.id) 
                            FROM App\Entity\Submission ${inner}_submission
                            INNER JOIN App\Entity\Movie ${inner}_movie WITH ${inner}_movie.id = movie.id AND ${inner}_movie.id = ${inner}_submission.movie
                        )";

                        switch ($filter['totalType']) {
                            case 'eq':
                                $qb->andWhere($e->eq($query, ":$key"));
                                break;
                            case 'more':
                                $qb->andWhere($e->gt($query, ":$key"));
                                break;
                            case 'more-eq':
                                $qb->andWhere($e->gte($query, ":$key"));
                                break;
                            case 'less-eq':
                                $qb->andWhere($e->lte($query, ":$key"));
                                break;
                            case 'less':
                                $qb->andWhere($e->lt($query, ":$key"));
                                break;
                            default:
                                continue;
                        }

                        $qb->setParameter($key, $value);

                    }
                    break;
            }
        }

        if ($order != null) {
            if (!$order) {
                $order = ['id' => 'DESC'];
            }

            foreach ($order as $key => $value) {
                switch ($key) {
                    case 'id':
                        $qb->orderBy('movie.id', $value);
                        break;
                }
            }
        }

        return $qb;
    }

    /**
     * @param array $filter
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('movie.id'));

        return $qb->getQuery()
            ->useQueryCache(true)
//            ->useResultCache(true, 3600 * 24, md5(json_encode($filter)))
            ->getSingleScalarResult();
    }
}