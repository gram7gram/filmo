<?php

namespace App\Repository;

use App\Entity\Movie;
use App\Entity\SubmissionStatus;
use Doctrine\ORM\EntityRepository;

class SubmissionRepository extends EntityRepository
{
    /**
     * @param mixed $id
     * @param null $lockMode
     * @param null $lockVersion
     * @return Movie|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        $qb = $this->createFilterQuery();
        $e = $qb->expr();

        $qb->where($e->eq('submission.id', ':id'))
            ->setParameter('id', $id);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param array $filter
     * @param int $page
     * @param int $limit
     * @return array
     */
    public function findByFilter($filter = [], $page = 0, $limit = 0)
    {
        $qb = $this->createFilterQuery($filter);

        $qb->orderBy('submission.id', 'DESC');

        if ($page > 0 && $limit > 0) {
            $qb->setMaxResults($limit)
                ->setFirstResult($limit * ($page - 1));
        }

        return $qb->getQuery()
            ->useQueryCache(true)
//            ->useResultCache(true, 3600 * 24, md5(json_encode($filter) . $page . $limit))
            ->getResult();
    }

    private function createFilterQuery($filter = [])
    {
        $qb = $this->createQueryBuilder('submission');
        $e = $qb->expr();

        $qb
            ->addSelect('category')
            ->addSelect('movie')
            ->addSelect('user');

        $qb
            ->join('submission.category', 'category')
            ->join('submission.movie', 'movie')
            ->join('submission.user', 'user');

        foreach ($filter as $key => $value) {
            switch ($key) {
                case 'obsolete':
                    $qb->andWhere($e->andX()
                        ->add($e->gte('(DATE_DIFF(CURRENT_DATE(), submission.statusUpdatedAt))', ":$key"))
                        ->add($e->in('submission.status', ':statuses')))
                        ->setParameter($key, $value)
                        ->setParameter('statuses', [SubmissionStatus::WITHDRAWN, SubmissionStatus::REJECTED]);
                    break;
                case 'dateValue':
                    if (isset($filter['dateType'])) {
                        $dateValueFrom = \DateTime::createFromFormat('Y-m-d', $value);
                        $dateValueTo = \DateTime::createFromFormat('Y-m-d', $value);
                        if ($dateValueFrom && $dateValueTo) {

                            $dateValueFrom->setTime(0, 0, 0);
                            $dateValueTo->setTime(23, 59, 59);

                            switch ($filter['dateType']) {
                                case 'eq':
                                    $qb->andWhere($e->andX()
                                        ->add($e->gte('submission.statusUpdatedAt', ':' . $key . '_from'))
                                        ->add($e->lte('submission.statusUpdatedAt', ':' . $key . '_to'))
                                    );
                                    $qb->setParameter($key . '_from', $dateValueFrom);
                                    $qb->setParameter($key . '_to', $dateValueTo);
                                    break;
                                case 'more':
                                    $qb->andWhere($e->gt('submission.statusUpdatedAt', ":$key"));
                                    $qb->setParameter($key, $dateValueTo);
                                    break;
                                case 'more-eq':
                                    $qb->andWhere($e->gte('submission.statusUpdatedAt', ":$key"));
                                    $qb->setParameter($key, $dateValueFrom);
                                    break;
                                case 'less-eq':
                                    $qb->andWhere($e->lte('submission.statusUpdatedAt', ":$key"));
                                    $qb->setParameter($key, $dateValueTo);
                                    break;
                                case 'less':
                                    $qb->andWhere($e->lt('submission.statusUpdatedAt', ":$key"));
                                    $qb->setParameter($key, $dateValueFrom);
                                    break;
                                default:
                                    continue;
                            }
                        }
                    }
                    break;
                case 'user':
                    $qb->andWhere($e->eq('user.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'status':
                    $qb->andWhere($e->eq('submission.status', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'movie':
                    $qb->andWhere($e->eq('movie.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
                case 'category':
                    $qb->andWhere($e->eq('category.id', ":$key"))
                        ->setParameter($key, $value);
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param array $filter
     * @return int
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countByFilter($filter = [])
    {
        $qb = $this->createFilterQuery($filter);
        $e = $qb->expr();

        $qb->select($e->countDistinct('submission.id'));

        return $qb->getQuery()
            ->useQueryCache(true)
//            ->useResultCache(true, 3600 * 24, md5(json_encode($filter)))
            ->getSingleScalarResult();
    }
}