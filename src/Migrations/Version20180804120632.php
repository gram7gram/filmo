<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180804120632 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('UPDATE submissions SET status_updated_at = created_at WHERE status_updated_at IS NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
