<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

final class Version20180722155605 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('insert into users(email, name, password, is_active, created_at, is_admin) values (\'admin\', \'Admin\', \'$2y$12$V/sFLNxZPUOZ3QowrsezlOeiYrr.J3NBw7xnQjpyFijmE89IyrrzS\', true, now(), true)');
        $this->addSql('insert into users(email, name, password, is_active, created_at, is_admin) values (\'user\', \'User\', \'$2y$12$V/sFLNxZPUOZ3QowrsezlOeiYrr.J3NBw7xnQjpyFijmE89IyrrzS\', true, now(), false)');
    }

    public function down(Schema $schema): void
    {
    }
}
