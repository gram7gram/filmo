<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

final class Version20180726113617 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("INSERT INTO categories (name) VALUES ('Arabic')");
        $this->addSql("INSERT INTO categories (name) VALUES ('Hollywood')");
        $this->addSql("INSERT INTO categories (name) VALUES ('Bollywood')");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
