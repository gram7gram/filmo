<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181003111330 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE users ALTER is_admin DROP DEFAULT');

        $this->addSql('ALTER TABLE users ADD password_token VARCHAR(128) DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD password_token_expires_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE users ADD password_token_requested_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9BEAB6C24 ON users (password_token)');
        $this->addSql(' ALTER TABLE users ADD birthday DATE DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
    }
}
