<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

final class Version20180722155505 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE users (id SERIAL NOT NULL, password VARCHAR(64) NOT NULL, is_admin BOOLEAN NOT NULL, public_token VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, is_active BOOLEAN NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9E7927C74 ON users (email)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_1483A5E9AE981E3B ON users (public_token)');
        $this->addSql('CREATE TABLE categories (id SERIAL NOT NULL, name TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3AF346685E237E06 ON categories (name)');
        $this->addSql('CREATE TABLE submissions (id SERIAL NOT NULL, user_id INT NOT NULL, movie_id INT NOT NULL, video_name VARCHAR(255) NOT NULL, category_id INT NOT NULL, status_updated_by_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, status VARCHAR(255) NOT NULL, status_updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, is_withdrawn BOOLEAN NOT NULL, withdrawn_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_3F6169F7A76ED395 ON submissions (user_id)');
        $this->addSql('CREATE INDEX IDX_3F6169F78F93B6FC ON submissions (movie_id)');
        $this->addSql('CREATE INDEX IDX_3F6169F712469DE2 ON submissions (category_id)');
        $this->addSql('CREATE INDEX IDX_3F6169F7A4A5EA4B ON submissions (status_updated_by_id)');
        $this->addSql('CREATE TABLE movies (id SERIAL NOT NULL, category_id INT NOT NULL, created_by_id INT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, name TEXT NOT NULL, is_hidden BOOLEAN NOT NULL, poster_name TEXT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_C61EED3012469DE2 ON movies (category_id)');
        $this->addSql('CREATE INDEX IDX_C61EED30B03A8386 ON movies (created_by_id)');
        $this->addSql('ALTER TABLE submissions ADD CONSTRAINT FK_3F6169F7A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE submissions ADD CONSTRAINT FK_3F6169F78F93B6FC FOREIGN KEY (movie_id) REFERENCES movies (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE submissions ADD CONSTRAINT FK_3F6169F712469DE2 FOREIGN KEY (category_id) REFERENCES categories (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE submissions ADD CONSTRAINT FK_3F6169F7A4A5EA4B FOREIGN KEY (status_updated_by_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE movies ADD CONSTRAINT FK_C61EED3012469DE2 FOREIGN KEY (category_id) REFERENCES categories (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE movies ADD CONSTRAINT FK_C61EED30B03A8386 FOREIGN KEY (created_by_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
    }
}
