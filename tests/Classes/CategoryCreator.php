<?php

namespace App\Tests\Classes;

use App\Entity\Category;

trait CategoryCreator
{

    /**
     * @return Category
     */
    public function createCategory()
    {
        $entity = new Category();

        $entity->setName(md5(uniqid()));

        return $entity;
    }

}