<?php

namespace App\Tests\Classes;

use App\Entity\Movie;

trait MovieCreator
{
    use CategoryCreator;

    /**
     * @return Movie
     */
    public function createMovie()
    {
        $category = $this->createCategory();

        $entity = new Movie();
        $entity->setName(md5(uniqid()));
        $entity->setPosterName(md5(uniqid()));
        $entity->setIsHidden(true);
        $entity->setCategory($category);

        return $entity;
    }

}