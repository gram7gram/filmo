<?php

namespace App\Tests\Controller;

use App\Service\UserService;
use App\Tests\Classes\WebTestCase;

/**
 * @covers \App\Controller\LoginController
 */
class LoginControllerTest extends WebTestCase
{

    public function testActivatePageOpens()
    {
        $client = $this->createUnauthorizedClient();

        $userService = $client->getContainer()->get(UserService::class);
        $user = $userService->create([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@mail.com',
            'password' => '12345',
            'isActive' => false,
            'birthday' => date('2000-m-d'),
        ]);

        $client->request('GET', "/activate/" . $user->getPublicToken());

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());
    }
}