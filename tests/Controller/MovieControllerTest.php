<?php

namespace App\Tests\Controller;

use App\Entity\Movie;
use App\Tests\Classes\WebTestCase;

/**
 * @covers \App\Controller\MovieController
 */
class MovieControllerTest extends WebTestCase
{

    public function testPageNotOpensIfNotAuthenticated()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/movies");

        $response = $client->getResponse();

        $this->assertFalse($response->isSuccessful());
    }

    public function testPageOpens()
    {
        $client = $this->createAuthorizedClient();

        $client->request('GET', "/movies");

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());
    }

    public function testNewPageOpens()
    {
        $client = $this->createAuthorizedClient();

        $client->request('GET', "/movies/new");

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());
    }

    public function testEditPageOpens()
    {
        $client = $this->createAuthorizedClient();

        $em = $client->getContainer()->get('doctrine')->getManager();
        $movie = $em->getRepository(Movie::class)->findOneBy([]);

        $this->assertNotNull($movie, 'Missing movie');

        $client->request('GET', "/movies/" . $movie->getId());

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());
    }
}