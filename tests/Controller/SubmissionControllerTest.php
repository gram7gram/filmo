<?php

namespace App\Tests\Controller;

use App\Entity\Movie;
use App\Entity\Submission;
use App\Tests\Classes\WebTestCase;

/**
 * @covers \App\Controller\SubmissionController
 */
class SubmissionControllerTest extends WebTestCase
{

    public function testPageNotOpensIfNotAuthenticated()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/submissions");

        $response = $client->getResponse();

        $this->assertFalse($response->isSuccessful());
    }

    public function testPageOpens()
    {
        $client = $this->createAuthorizedClient();

        $client->request('GET', "/submissions");

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());
    }

    public function testNewPageOpens()
    {
        $client = $this->createAuthorizedClient();

        $em = $client->getContainer()->get('doctrine')->getManager();
        $entity = $em->getRepository(Movie::class)->findOneBy([
            'isHidden' => false
        ]);

        $this->assertNotNull($entity, 'Missing movie');

        $client->request('GET', "/movies/" . $entity->getId() . "/submission");

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());
    }

    public function testEditPageOpens()
    {
        $client = $this->createAuthorizedClient();

        $em = $client->getContainer()->get('doctrine')->getManager();
        $entity = $em->getRepository(Submission::class)->findOneBy([]);

        $this->assertNotNull($entity, 'Missing submission');

        $client->request('GET', "/submissions/" . $entity->getId());

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());
    }
}