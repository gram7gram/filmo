<?php

namespace App\Tests\Controller;

use App\Entity\Category;
use App\Entity\Movie;
use App\Entity\SubmissionStatus;
use App\Service\SubmissionService;
use App\Service\UserService;
use App\Tests\Classes\MovieCreator;
use App\Tests\Classes\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\SubmissionRESTController
 */
class SubmissionRESTControllerTest extends WebTestCase
{
    use MovieCreator;

    public function getProvider() {
        return [
            [[]],
            [[
                'category' => 1,
                'obsolete' => 5,
                'user' => 5,
                'movie' => 5,
                'status' => SubmissionStatus::SUBMITTED,
            ]],
            [[
                'dateType' => 'eq',
                'dateValue' => '2018-08-04',
            ]],
            [[
                'dateType' => 'more',
                'dateValue' => '2018-08-04',
            ]],
            [[
                'dateType' => 'more-eq',
                'dateValue' => '2018-08-04',
            ]],
            [[
                'dateType' => 'less',
                'dateValue' => '2018-08-04',
            ]],
            [[
                'dateType' => 'less-eq',
                'dateValue' => '2018-08-04',
            ]],
        ];
    }

    /**
     * @dataProvider getProvider
     * @param $filter
     * @throws \Exception
     */
    public function testGetsAction($filter)
    {
        $client = $this->createAuthorizedClient();

        $client->request('GET', "/api/v1/submissions", [
            'page' => 1,
            'filter' => $filter
        ]);

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['page']), 'Missing page');
        $this->assertTrue(isset($content['limit']), 'Missing limit');
        $this->assertTrue(isset($content['total']), 'Missing total');
        $this->assertTrue(isset($content['count']), 'Missing count');
        $this->assertTrue(isset($content['items']), 'Missing items');
    }

    public function testPostAction()
    {
        $client = $this->createAuthorizedClient();
        $em = $client->getContainer()->get('doctrine')->getManager();

        $root = $client->getContainer()->getParameter('kernel.project_dir') . '/public';
        $submissionDirectory = $client->getContainer()->getParameter('submission_dir');

        $user = $client->getContainer()->get(UserService::class)->getUser();
        $category = $em->getRepository(Category::class)->findOneBy([]);

        $movie = $this->createMovie();

        $movie->setCreatedBy($user);
        $movie->setCategory($category);

        $em->persist($movie);
        $em->flush();

        $name = 'test_' . md5(uniqid()) . '.mp4';
        $path = $root . $submissionDirectory . '/' . $name;

        file_put_contents($path, null);

        $client->request('POST', "/api/v1/submissions", [], [
            'video' => new UploadedFile($path, $name, 'video/mp4', null, true)
        ], [], json_encode([
            'content' => [
                'movie' => $movie->getId(),
                'category' => $category->getId()
            ]
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    public function testGetAction()
    {
        $client = $this->createAuthorizedClient();
        $em = $client->getContainer()->get('doctrine')->getManager();

        $root = $client->getContainer()->getParameter('kernel.project_dir') . '/public';
        $submissionDirectory = $client->getContainer()->getParameter('submission_dir');

        $submissionService = $client->getContainer()->get(SubmissionService::class);
        $category = $em->getRepository(Category::class)->findOneBy([]);
        $movie = $em->getRepository(Movie::class)->findOneBy([]);

        $name = 'test_' . md5(uniqid()) . '.mp4';
        $path = $root . $submissionDirectory . '/' . $name;

        file_put_contents($path, null);

        $videoFile = new UploadedFile($path, $name, 'video/mp4', null, true);

        $submission = $submissionService->create([
            'movie' => $movie->getId(),
            'category' => $category->getId()
        ], $videoFile);

        $client->request('GET', "/api/v1/submissions/" . $submission->getId());

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());
    }

    public function testPutAction()
    {
        $client = $this->createAuthorizedClient();
        $em = $client->getContainer()->get('doctrine')->getManager();

        $root = $client->getContainer()->getParameter('kernel.project_dir') . '/public';
        $submissionDirectory = $client->getContainer()->getParameter('submission_dir');

        $submissionService = $client->getContainer()->get(SubmissionService::class);
        $category = $em->getRepository(Category::class)->findOneBy([]);
        $movie = $em->getRepository(Movie::class)->findOneBy([]);

        $name = 'test_' . md5(uniqid()) . '.mp4';
        $path = $root . $submissionDirectory . '/' . $name;

        file_put_contents($path, null);

        $videoFile = new UploadedFile($path, $name, 'video/mp4', null, true);

        $submission = $submissionService->create([
            'movie' => $movie->getId(),
            'category' => $category->getId()
        ], $videoFile);

        $client->request('PUT', "/api/v1/submissions/" . $submission->getId(), [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], json_encode([
            'status' => SubmissionStatus::WITHDRAWN
        ]));

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());
    }

    public function testExportAction()
    {
        $client = $this->createAuthorizedClient();
        $em = $client->getContainer()->get('doctrine')->getManager();

        $category = $em->getRepository(Category::class)->findOneBy([]);

        $client->request('POST', "/api/v1/submissions/export", [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], json_encode([
            'filter' => [
                'category' => $category->getId()
            ]
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['url']), 'Missing url');
    }

    public function testImportAction()
    {
        $client = $this->createAuthorizedClient();
        $em = $client->getContainer()->get('doctrine')->getManager();

        $root = $client->getContainer()->getParameter('kernel.project_dir') . '/public';
        $submissionDirectory = $client->getContainer()->getParameter('submission_dir');

        $submissionService = $client->getContainer()->get(SubmissionService::class);
        $category = $em->getRepository(Category::class)->findOneBy([]);
        $movie = $em->getRepository(Movie::class)->findOneBy([]);

        $name = 'test_' . md5(uniqid()) . '.mp4';
        $path = $root . $submissionDirectory . '/' . $name;

        file_put_contents($path, null);

        $videoFile = new UploadedFile($path, $name, 'video/mp4', null, true);

        $submission = $submissionService->create([
            'movie' => $movie->getId(),
            'category' => $category->getId()
        ], $videoFile);

        $name = 'test_' . md5(uniqid()) . '.csv';
        $path = '/tmp/' . $name;

        file_put_contents($path, $submission->getId() . ',' . SubmissionStatus::REJECTED);

        $client->request('POST', "/api/v1/submissions/import", [], [
            'import' => new UploadedFile($path, $name, 'text/plain', null, true)
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }

    public function testObsoleteAction()
    {
        $client = $this->createAuthorizedClient();

        $client->request('DELETE', "/api/v1/submissions/obsolete");

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }
}