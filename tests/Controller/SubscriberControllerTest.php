<?php

namespace App\Tests\Controller;

use App\Entity\User;
use App\Tests\Classes\WebTestCase;

/**
 * @covers \App\Controller\SubscriberController
 */
class SubscriberControllerTest extends WebTestCase
{

    public function testPageNotOpensIfNotAuthenticated()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/subscribers");

        $response = $client->getResponse();

        $this->assertFalse($response->isSuccessful());
    }

    public function testPageOpens()
    {
        $client = $this->createAuthorizedClient();

        $client->request('GET', "/subscribers");

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());
    }

    public function testNewPageOpens()
    {
        $client = $this->createAuthorizedClient();

        $client->request('GET', "/subscribers/new");

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());
    }

    public function testProfilePageOpens()
    {
        $client = $this->createAuthorizedClient();

        $client->request('GET', "/profile");

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());
    }

    public function testEditPageOpens()
    {
        $client = $this->createAuthorizedClient();

        $em = $client->getContainer()->get('doctrine')->getManager();
        $entity = $em->getRepository(User::class)->findOneBy([]);

        $this->assertNotNull($entity, 'Missing user');

        $client->request('GET', "/subscribers/" . $entity->getId());

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());
    }
}