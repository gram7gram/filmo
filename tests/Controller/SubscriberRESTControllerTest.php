<?php

namespace App\Tests\Controller;

use App\Entity\User;
use App\Tests\Classes\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\SubscriberRESTController
 */
class SubscriberRESTControllerTest extends WebTestCase
{

    /**
     * TODO
     *
     * @throws \Exception
     */
    public function todoPutAction()
    {
        $client = $this->createAuthorizedClient();

        $em = $client->getContainer()->get('doctrine')->getManager();

        $entity = $em->getRepository(User::class)->findOneBy([
            'isActive' => true
        ]);

        $this->assertNotNull($entity, 'Missing user');

        $client = $this->createAuthorizedClient($entity->getUsername());

        $client->request('PUT', "/api/v1/subscribers/" . $entity->getId(), [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], json_encode([
            'name' => md5(uniqid())
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testGetsAction()
    {
        $client = $this->createAuthorizedClient();

        $client->request('GET', "/api/v1/subscribers", [
            'page' => 1,
            'limit' => 5
        ]);

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['page']), 'Missing page');
        $this->assertTrue(isset($content['limit']), 'Missing limit');
        $this->assertTrue(isset($content['total']), 'Missing total');
        $this->assertTrue(isset($content['count']), 'Missing count');
        $this->assertTrue(isset($content['items']), 'Missing items');
    }

    public function testPostAction()
    {
        $client = $this->createAuthorizedClient();

        $client->request('POST', "/api/v1/subscribers", [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], json_encode([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()), '@mail.com',
            'password' => md5(uniqid()),
            'birthday' => date('2000-m-d')
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    public function testGetAction()
    {
        $client = $this->createAuthorizedClient();
        $em = $client->getContainer()->get('doctrine')->getManager();

        $entity = $em->getRepository(User::class)->findOneBy([]);

        $this->assertNotNull($entity, 'Missing user');

        $client->request('GET', "/api/v1/subscribers/" . $entity->getId());

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}