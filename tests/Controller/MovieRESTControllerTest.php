<?php

namespace App\Tests\Controller;

use App\Entity\Category;
use App\Service\MovieService;
use App\Tests\Classes\CategoryCreator;
use App\Tests\Classes\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\MovieRESTController
 */
class MovieRESTControllerTest extends WebTestCase
{
    use CategoryCreator;

    public function getProvider() {
        return [
            [[]],
            [[
                'isHidden' => true,
                'category' => 5,
                'letter' => 'X',
            ]],
            [[
                'acceptedType' => 'eq',
                'acceptedCount' => 10,
            ]],
            [[
                'acceptedType' => 'more',
                'acceptedCount' => 10,
            ]],
            [[
                'acceptedType' => 'more-eq',
                'acceptedCount' => 10,
            ]],
            [[
                'acceptedType' => 'less',
                'acceptedCount' => 10,
            ]],
            [[
                'acceptedType' => 'less-eq',
                'acceptedCount' => 10,
            ]],
            [[
                'totalType' => 'eq',
                'totalCount' => 10,
            ]],
            [[
                'totalType' => 'more',
                'totalCount' => 10,
            ]],
            [[
                'totalType' => 'more-eq',
                'totalCount' => 10,
            ]],
            [[
                'totalType' => 'less',
                'totalCount' => 10,
            ]],
            [[
                'totalType' => 'less-eq',
                'totalCount' => 10,
            ]],
        ];
    }

    /**
     * @dataProvider getProvider
     * @param $filter
     * @throws \Exception
     */
    public function testGetsAction($filter)
    {
        $client = $this->createAuthorizedClient();

        $client->request('GET', "/api/v1/movies", [
            'page' => 1,
            'limit' => 5,
            'filter' => $filter
        ]);

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['page']), 'Missing page');
        $this->assertTrue(isset($content['limit']), 'Missing limit');
        $this->assertTrue(isset($content['total']), 'Missing total');
        $this->assertTrue(isset($content['count']), 'Missing count');
        $this->assertTrue(isset($content['items']), 'Missing items');
    }

    public function testPostAction()
    {
        $client = $this->createAuthorizedClient();
        $em = $client->getContainer()->get('doctrine')->getManager();
        $root = $client->getContainer()->getParameter('kernel.project_dir') . '/public';
        $posterDirectory = $client->getContainer()->getParameter('poster_dir');

        $category = $em->getRepository(Category::class)->findOneBy([]);

        $poster = 'test_' . md5(uniqid()) . '.jpg';
        $path = $root . $posterDirectory . '/' . $poster;

        file_put_contents($path, null);

        $posterFile = new UploadedFile($path, $poster, 'image/jpg', null, true);

        $client->request('POST', "/api/v1/movies", [], [
            'poster' => $posterFile
        ], [], json_encode([
            'name' => md5(uniqid()),
            'isHidden' => false,
            'category' => $category->getId()
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    public function testGetAction()
    {
        $client = $this->createAuthorizedClient();
        $em = $client->getContainer()->get('doctrine')->getManager();
        $service = $client->getContainer()->get(MovieService::class);
        $root = $client->getContainer()->getParameter('kernel.project_dir') . '/public';
        $posterDirectory = $client->getContainer()->getParameter('poster_dir');

        $category = $em->getRepository(Category::class)->findOneBy([]);

        $poster = 'test_' . md5(uniqid()) . '.jpg';

        $path = $root . $posterDirectory . '/' . $poster;

        file_put_contents($path, null);

        $posterFile = new UploadedFile($path, $poster, 'image/jpg', null, true);

        $movie = $service->create([
            'name' => md5(uniqid()),
            'isHidden' => false,
            'category' => $category->getId()
        ], $posterFile);

        $client->request('GET', "/api/v1/movies/" . $movie->getId());

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }

    public function testPutAction()
    {
        $client = $this->createAuthorizedClient();
        $em = $client->getContainer()->get('doctrine')->getManager();
        $service = $client->getContainer()->get(MovieService::class);
        $root = $client->getContainer()->getParameter('kernel.project_dir') . '/public';
        $posterDirectory = $client->getContainer()->getParameter('poster_dir');

        $category = $em->getRepository(Category::class)->findOneBy([]);

        $poster = 'test_' . md5(uniqid()) . '.jpg';

        $path = $root . $posterDirectory . '/' . $poster;

        file_put_contents($path, null);

        $posterFile = new UploadedFile($path, $poster, 'image/jpg', null, true);

        $movie = $service->create([
            'name' => md5(uniqid()),
            'isHidden' => false,
            'category' => $category->getId()
        ], $posterFile);

        $client->request('PUT', "/api/v1/movies/" . $movie->getId(), [], [], [], json_encode([
            'category' => $category->getId()
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['category']['id']), 'Missing category.id');
        $this->assertEquals($content['category']['id'], $category->getId(), 'Invalid category.id');
    }

    public function testExportAction()
    {
        $client = $this->createAuthorizedClient();
        $em = $client->getContainer()->get('doctrine')->getManager();

        $category = $em->getRepository(Category::class)->findOneBy([]);

        $client->request('POST', "/api/v1/movies/export", [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], json_encode([
            'filter' => [
                'category' => $category->getId()
            ]
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());

        $content = json_decode($response->getContent(), true);

        $this->assertTrue(isset($content['url']), 'Missing url');
    }

    public function testImportAction()
    {
        $client = $this->createAuthorizedClient();

        $root = $client->getContainer()->getParameter('kernel.project_dir') . '/public';
        $posterDirectory = $client->getContainer()->getParameter('poster_dir');

        $name = 'test_' . md5(uniqid()) . '.csv';
        $path = '/tmp/' . $name;

        $poster = 'test_' . md5(uniqid()) . '.jpg';
        $posterPath = $root . $posterDirectory . '/' . $poster;

        file_put_contents($posterPath, null);

        file_put_contents($path, ',Test Movie 1,1,0,' . $poster);

        $csv = new UploadedFile($path, $name, 'text/plain', null, true);

        $client->request('POST', "/api/v1/movies/import", [], [
            'import' => $csv
        ]);

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_NO_CONTENT, $response->getStatusCode());
    }
}