<?php

namespace App\Tests\Controller;

use App\Tests\Classes\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

/**
 * @covers \App\Controller\RegisterRESTController
 */
class RegisterRESTControllerTest extends WebTestCase
{

    public function test_register_not_allowed_if_less_then_18_yo()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('POST', "/api/v1/register", [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], json_encode([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@gmail.com',
            'password' => md5(uniqid()),
            'birthday' => date('Y-m-d')
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $response->getStatusCode());
    }

    public function test_register_allowed_if_more_then_18_yo()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('POST', "/api/v1/register", [], [], [
            'CONTENT_TYPE' => 'application/json'
        ], json_encode([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()) . '@gmail.com',
            'password' => md5(uniqid()),
            'birthday' => date('2000-m-d')
        ]));

        $response = $client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }
}