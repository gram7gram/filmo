<?php

namespace App\Tests\Controller;

use App\Service\UserService;
use App\Tests\Classes\WebTestCase;

/**
 * @covers \App\Controller\IndexController
 */
class IndexControllerTest extends WebTestCase
{

    public function pageProvider()
    {
        return [
            ['/'],
            ['/tutorial'],
            ['/terms'],
            ['/faq'],
            ['/contact-us'],
            ['/contact-support'],
            ['/login'],
            ['/register'],
            ['/reset-password'],
        ];
    }

    public function testEntryPageOpens()
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', "/entry");

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());
        $this->assertTrue(strpos($response->headers->get('location'), '/login') !== -1);
    }

    public function testEntryAsAdminPageOpens()
    {
        $client = $this->createAuthorizedClient();

        $client->request('GET', "/entry");

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());
        $this->assertEquals('/movies', $response->headers->get('location'));
    }

    public function testEntryAsNotAdminPageOpens()
    {
        $client = $this->createUnauthorizedClient();

        $userService = $client->getContainer()->get(UserService::class);

        $user = $userService->create([
            'name' => md5(uniqid()),
            'email' => md5(uniqid()), '@mail.com',
            'password' => md5(uniqid()),
            'birthday' => date('2000-m-d'),
        ]);

        $client = $this->createAuthorizedClient($user->getUsername());

        $client->request('GET', "/entry");

        $response = $client->getResponse();

        $this->assertTrue($response->isRedirection());
        $this->assertEquals('/movies', $response->headers->get('location'));
    }

    /**
     * @param $url
     *
     * @dataProvider pageProvider
     */
    public function testPageOpens($url)
    {
        $client = $this->createUnauthorizedClient();

        $client->request('GET', $url);

        $response = $client->getResponse();

        $this->assertTrue($response->isSuccessful());
        $this->assertFalse($response->isRedirection());
    }

}